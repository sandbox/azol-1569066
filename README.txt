This module performs a one-way migration of YaBB 2.5 boards, users, topics,
polls and PMs into Drupal 7 Forum/Poll/Private Messages.
 
At the moment only users and boards data migration is partially working.
Do NOT use this module on production site!
 
REQUIREMENTS----------

Drupal core: 7.x
YaBB version: 2.5
 
Module dependency:
 - (core) Forum - optional, required only if you migrate boards, topics and polls;
 - (core) Poll - optional, required only if you migrate polls;
 - (contrib) Private Messages - optional, required only if you migrate PMs.

INSTALL---------------

Install as you would do normally with contrib module

USE-------------------

After enabling the module, go to admin/structure/yabb

NOTES-----------------

Additional notes on the migration process and data transfer:

USERS:
 - only new users are being created in Drupal.
   It does not update existing users in any way,
   so better use a clean Drupal install to minimize possible naming conflicts;
 - at the moment, only user name (login), email
   and registration date are preserved;
 - all users will have to reset their passwords
   before they will be able to log in to new forum.
 - there is currently no functionality for user roles.
 
BOARDS:
 - it creates the boards using machine names from YaBB,
   then updates the taxonomy term titles to match the original board names;
 - the boards are created normally as sticky and/or locked,
   according to their original values in the YaBB forum.
 - no categories/hierarchies are preserved
   (which is simple to manually edit afterwards).
 - CAUTION: there are no access rules imported which means that
   all members will be able to view all boards and topics.
 
TOPICS:
 - during the migration, BBcode will be converted into HTML most likely,
   as I cannot find any decent BBcode WYSIWYG-compatible editor for Drupal 7.
 - the "Moved By" posts get ignored.